from setuptools import setup, find_packages

setup(name="pingdom_daemon",
      version="0.0.1",
      description="Daemon to index ping statistics into Elasticsearch.",
      scripts=["scripts/pdk-daemon"]
      )
